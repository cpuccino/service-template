import { transpileSchema } from "@middy/validator/transpile";

export const helloJSONSchema = {
	type: "object",
	required: ["body"],
	properties: {
		body: {
			type: "object",
			required: ["name"],
			properties: {
				name: {
					type: "string",
					minLength: 5,
					pattern: "[a-zA-Z]+#[0-9]*$"
				}
			}
		}
	}
} as const;

export const helloSchema = transpileSchema(helloJSONSchema);
