import { stringify } from "@utilities/json";

export type JSONResponse = {
	statusCode: number;
	body: string;
};

export type SuccessStatusCode = 200 | 201;
export type ErrorStatusCode = 400 | 404 | 500 | 503;

export type SuccessPayload<T = {}> = { data?: T } & { errors?: never };
export type ErrorPayload = { errors: { [key: string]: string | number; message: string }[] };

type StatusCode = SuccessStatusCode | ErrorStatusCode;
type Payload<T, U> = U extends SuccessStatusCode ? SuccessPayload<T> : ErrorPayload;

export const formatJSONResponse = <T, U extends StatusCode, V extends Payload<T, U>>(
	statusCode: U,
	payload: V
): JSONResponse => ({
	statusCode,
	body: stringify(payload)
});
