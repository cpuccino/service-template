const circularReplacer = () => {
	const seen = new WeakSet();

	return (_: string, value?: string | number | boolean | object) => {
		if (typeof value === "object" && value !== null) {
			if (seen.has(value)) return "[CIRCULAR_REFERENCE]";
			seen.add(value);
		}

		return value;
	};
};

export const stringify = (data?: string | number | boolean | object) =>
	JSON.stringify(data, circularReplacer());
