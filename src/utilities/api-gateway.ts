import type { APIGatewayProxyEvent, APIGatewayProxyResult, Handler } from "aws-lambda";
import type { FromSchema, JSONSchema } from "json-schema-to-ts";

export type ValidatedApiGatewayHandler<S extends JSONSchema> = Handler<
	Omit<APIGatewayProxyEvent, "body"> & FromSchema<S>,
	APIGatewayProxyResult
>;
