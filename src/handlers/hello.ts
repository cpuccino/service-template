import type { AWS } from "@serverless/typescript";
import type { ValidatedApiGatewayHandler } from "@utilities/api-gateway";

import middy from "@middy/core";
import validator from "@middy/validator";
import jsonBodyParser from "@middy/http-json-body-parser";

import { helloJSONSchema, helloSchema } from "@schemas/hello";
import { sanitizeErrors } from "@middlewares/sanitize-errors";
import { formatJSONResponse } from "@utilities/response";

const main: ValidatedApiGatewayHandler<typeof helloJSONSchema> = async event => {
	const { name } = event.body;

	return formatJSONResponse(200, {
		data: {
			message: `Hello ${name}, welcome to the exciting world of serverless!`
		}
	});
};

export const handler = middy(main)
	.use(jsonBodyParser())
	.use(validator({ eventSchema: helloSchema }))
	.use(sanitizeErrors());

export const handlerConfiguration: AWS["functions"] = {
	hello: {
		handler: "src/handlers/hello.handler",
		events: [
			{
				httpApi: {
					method: "POST",
					path: "/hello"
				}
			}
		]
	}
};
