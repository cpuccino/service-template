import type { JSONResponse } from "@utilities/response";
import type { MiddlewareObj } from "@middy/core";

import { formatJSONResponse } from "@utilities/response";

type SanitizeErrorMiddlewareError = Error & {
	cause?: { instancePath: string; message: string }[];
};
type SanitizeErrorMiddlewareObj = MiddlewareObj<{}, JSONResponse, Error>;

const isValidationError = (error: Error) => error.name === "BadRequestError";

const createValidationError = (error: SanitizeErrorMiddlewareError) => {
	const { cause } = error;
	const defaultValidationError = [{ message: "Invalid parameter." }];

	if (!cause || !cause.length) {
		return formatJSONResponse(400, { errors: defaultValidationError });
	}

	return formatJSONResponse(400, {
		errors: cause.map(detail => {
			const { instancePath, message } = detail;

			return {
				message: `${instancePath ? `${instancePath} ` : instancePath}${message}.`
			};
		})
	});
};

const createInternalServerError = () =>
	formatJSONResponse(500, {
		errors: [
			{
				message: "Something went wrong. Try again later."
			}
		]
	});

export const sanitizeErrors = (): Required<Pick<SanitizeErrorMiddlewareObj, "onError">> => ({
	onError: request => {
		const { error } = request;

		if (!error) {
			request.response = createInternalServerError();
			return request.response;
		}

		if (isValidationError(error)) {
			request.response = createValidationError(error as SanitizeErrorMiddlewareError);
			return request.response;
		}

		request.response = createInternalServerError();
		return request.response;
	}
});
