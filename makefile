all: help

# dev: Run the application in dev mode (watch changes)
dev:
	yarn serverless offline --reloadHandler 

# build: Build the application.
build:
	yarn serverless package

# test: Runs tests and exports coverage.
test:
	yarn jest --config jest.config.json --coverage

deploy:
	yarn serverless deploy

# lint: Runs all linters.
lint:
	yarn eslint --ignore-path .gitignore .
	make cfn-lint

# cfn-lint: Runs cloudformation linter.
cfn-lint:
	make build
	cfn-lint .serverless/cloudformation-template-*.json \
		-a cfn_lint_serverless.rules

# format: Runs prettier.
format:
	yarn prettier --ignore-path .gitignore --write .

# help: Display the help page.
help:
	@awk 'BEGIN{FS=":"}{if(/^# [a-z.A-Z_-]+:.*/){printf "%-30s %s\n",substr($$1, 3), $$2 }}' $(MAKEFILE_LIST) | sort
