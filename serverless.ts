import type { AWS } from "@serverless/typescript";

import { handlerConfiguration as helloHandlerConfiguration } from "@handlers/hello";

const serverlessConfiguration: AWS = {
	service: "serverless-typescript-node-template",
	frameworkVersion: "3",
	plugins: ["serverless-esbuild", "serverless-offline"],
	provider: {
		name: "aws",
		runtime: "nodejs18.x",
		apiGateway: {
			minimumCompressionSize: 1024,
			shouldStartNameWithService: true
		},
		environment: {
			AWS_NODEJS_CONNECTION_REUSE_ENABLED: "1",
			NODE_OPTIONS: "--enable-source-maps --stack-trace-limit=1000"
		},
		logs: {
			httpApi: true
		},
		logRetentionInDays: 30
	},
	functions: {
		...helloHandlerConfiguration
	},
	package: { individually: true },
	custom: {
		esbuild: {
			bundle: true,
			minify: false,
			sourcemap: true,
			exclude: ["aws-sdk"],
			target: "node18",
			define: { "require.resolve": undefined },
			platform: "node",
			concurrency: 10,
			watch: {
				pattern: "src/**/*.ts"
			}
		},
		offline: {
			useChildProcesses: true
		}
	}
};

module.exports = serverlessConfiguration;
