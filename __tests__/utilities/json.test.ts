import { stringify } from "@utilities/json";

describe("Utilities: json", () => {
	describe("stringify", () => {
		describe("Given a piece of data", () => {
			describe(`And the data doesn't contain a circular reference`, () => {
				const data = { message: "Success" };

				test("Then it should return the stringified data", () => {
					const stringifiedData = stringify(data);
					expect(stringifiedData).toEqual(JSON.stringify({ message: "Success" }));
				});
			});

			describe("And the data contains a circular reference", () => {
				const data: { [key: string]: string | object } = { message: "Success" };
				data.circularReference = data;

				test("Then it should redact the circular reference property from the stringified data", () => {
					const stringifiedData = stringify(data);
					expect(stringifiedData).toEqual(
						JSON.stringify({
							message: "Success",
							circularReference: "[CIRCULAR_REFERENCE]"
						})
					);
				});
			});
		});
	});
});
