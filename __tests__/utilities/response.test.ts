import type { SuccessPayload } from "@utilities/response";
import { formatJSONResponse } from "@utilities/response";

describe("Utilities: response", () => {
	describe("formatJSONResponse", () => {
		describe("Given a statusCode and a payload", () => {
			const payload: SuccessPayload = { data: { message: "Success" } };

			test("Then it should return the statusCode and a stringified body containing the payload", () => {
				const { statusCode, body } = formatJSONResponse(200, payload);

				expect(statusCode).toEqual(200);
				expect(body).toEqual(JSON.stringify({ data: { message: "Success" } }));
			});
		});

		describe("Given a statusCode and a payload with a circular reference", () => {
			const payload: SuccessPayload = { data: { message: "Success" } };
			(payload.data as { circularReference: SuccessPayload }).circularReference = payload;

			test("Then it should redact the payload property with a circular reference", () => {
				const { statusCode, body } = formatJSONResponse(200, payload);

				expect(statusCode).toEqual(200);
				expect(body).toEqual(
					JSON.stringify({
						data: { message: "Success", circularReference: "[CIRCULAR_REFERENCE]" }
					})
				);
			});
		});
	});
});
