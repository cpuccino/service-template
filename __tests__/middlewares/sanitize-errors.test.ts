import type { Context } from "aws-lambda";

import { sanitizeErrors } from "@middlewares/sanitize-errors";
import { formatJSONResponse } from "@utilities/response";

describe("Middleware: sanitize-errors", () => {
	describe("sanitizeErrors", () => {
		// @ts-ignore
		const context: Context = {};
		const request = {
			event: {},
			context,
			response: null,
			error: null,
			internal: {}
		};

		describe("Given an error occurs and triggers the onError hook", () => {
			describe(`And there's no error object included in the request`, () => {
				test("Then it should throw an internal server error", () => {
					const response = sanitizeErrors().onError(request);

					expect(response).toEqual(
						formatJSONResponse(500, {
							errors: [
								{
									message: "Something went wrong. Try again later."
								}
							]
						})
					);
				});
			});

			describe("And the error is a validation error", () => {
				describe("And the error cause is empty", () => {
					const errorWithoutCause = {
						name: "BadRequestError",
						message: "Event object failed validation",
						cause: []
					};

					test("Then it should throw a generic Invalid parameter error", () => {
						const response = sanitizeErrors().onError({ ...request, error: errorWithoutCause });

						expect(response).toEqual(
							formatJSONResponse(400, {
								errors: [
									{
										message: "Invalid parameter."
									}
								]
							})
						);
					});
				});

				describe("And the error contains cause regarding the error", () => {
					describe("And the instancePath is not empty", () => {
						const errorWithCause = {
							name: "BadRequestError",
							message: "Event object failed validation",
							cause: [{ instancePath: "/body", message: "must have required property 'name'" }]
						};

						test("Then it should return an array of validation error messages", () => {
							const response = sanitizeErrors().onError({ ...request, error: errorWithCause });

							expect(response).toEqual(
								formatJSONResponse(400, {
									errors: [
										{
											message: "/body must have required property 'name'."
										}
									]
								})
							);
						});
					});

					describe("And the instancePath is empty", () => {
						const errorWithCause = {
							name: "BadRequestError",
							message: "Event object failed validation",
							cause: [{ instancePath: "", message: "must have required property 'headers'" }]
						};

						test("Then it should return an array of validation error messages", () => {
							const response = sanitizeErrors().onError({ ...request, error: errorWithCause });

							expect(response).toEqual(
								formatJSONResponse(400, {
									errors: [
										{
											message: "must have required property 'headers'."
										}
									]
								})
							);
						});
					});
				});
			});

			describe("And the error is unhandled", () => {
				const unhandledError: Error = {
					name: "UnhandledErrorException",
					message: "Unhandled Error."
				};

				test("Then it should throw an internal server error", () => {
					const response = sanitizeErrors().onError({ ...request, error: unhandledError });

					expect(response).toEqual(
						formatJSONResponse(500, {
							errors: [
								{
									message: "Something went wrong. Try again later."
								}
							]
						})
					);
				});
			});
		});
	});
});
