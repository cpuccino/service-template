import type { APIGatewayProxyEvent, Context } from "aws-lambda";

import { handler } from "@handlers/hello";

describe("Handler: hello", () => {
	describe("handler", () => {
		type HelloHandlerEvent = Omit<APIGatewayProxyEvent, "body"> & {
			body: {
				name: string;
			};
		};

		// @ts-ignore
		const baseEvent: Omit<APIGatewayProxyEvent, "body"> = { headers: {} };

		// @ts-ignore
		const context: Context = {};

		describe("Given an event", () => {
			describe("And all the required parameters are valid", () => {
				const event: HelloHandlerEvent = {
					...baseEvent,
					body: {
						name: "user#1234"
					}
				};

				test("Then it should throw a validation error", async () => {
					const response = await handler(event, context);

					expect(response).toEqual({
						statusCode: 200,
						body: JSON.stringify({
							data: {
								message: "Hello user#1234, welcome to the exciting world of serverless!"
							}
						})
					});
				});
			});

			describe("And the parameter /body is not given", () => {
				// @ts-ignore
				const event: HelloHandlerEvent = {
					...baseEvent
				};

				test("Then it should throw a validation error", async () => {
					const response = await handler(event, context);

					expect(response).toEqual({
						statusCode: 400,
						body: JSON.stringify({
							errors: [
								{
									message: "must have required property 'body'."
								}
							]
						})
					});
				});
			});

			describe("And the parameter /body/name is not given", () => {
				const event: HelloHandlerEvent = {
					...baseEvent,
					// @ts-ignore
					body: {}
				};

				test("Then it should throw a validation error", async () => {
					const response = await handler(event, context);

					expect(response).toEqual({
						statusCode: 400,
						body: JSON.stringify({
							errors: [
								{
									message: "/body must have required property 'name'."
								}
							]
						})
					});
				});
			});

			describe("And the parameter /body/name must not have fewer than 5 characters", () => {
				const event: HelloHandlerEvent = {
					...baseEvent,
					body: { name: "a#1" }
				};

				test("Then it should throw a validation error", async () => {
					const response = await handler(event, context);

					expect(response).toEqual({
						statusCode: 400,
						body: JSON.stringify({
							errors: [
								{
									message: "/body/name must NOT have fewer than 5 characters."
								}
							]
						})
					});
				});
			});

			describe(`And the parameter /body/name doesn't match the pattern`, () => {
				const event: HelloHandlerEvent = {
					...baseEvent,
					body: { name: "user12345" }
				};

				test("Then it should throw a validation error", async () => {
					const response = await handler(event, context);

					expect(response).toEqual({
						statusCode: 400,
						body: JSON.stringify({
							errors: [
								{
									message: '/body/name must match pattern "[a-zA-Z]+#[0-9]*$".'
								}
							]
						})
					});
				});
			});
		});
	});
});
