### Requirements

- node 18.x
- yarn 1.x
- pip 20.x
- cfn-lint
- cfn-lint-serverless
